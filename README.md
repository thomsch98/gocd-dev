# README #

## Usage ##
Just do a docker-compose up -d and you should be ready to go using your own local GoCD server with Java debugging enabled. 

## (Optional) Configuration ##

If required/preferred, you can enable authentication by modifying the "out of the box" setup. Just go to "Admin - Server Configuration" and add some values:

![gocd urls](./doc/admin-server-urls.png)

![credentials](./doc/admin-server-auth.png)

The "/etc/go/passwd" file was created for you when starting the go-server container. The credentials available are defined by the value of the environment vairable `GO_USERS` in the docker-compose file.


## Adding plugins ##

Getting a new plugin onto the server is as easy as copy your jar file to ./data/lb/plugins/external and wait a bit :-).
Using some environment variables, the server is watching for and integrating new plugins automatically.

## Environment ##

The compose-file contains a go-data container that takes care of creating required directories *on your host*.

> This is a *dev - env*, dude (and nothing like production/live/...).

## Java Remote Debugging Ports
5005 on the server-container, 5006 on the agent-container.

